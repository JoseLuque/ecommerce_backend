<?php

class Ruta{

	/*=============================================
	RUTA LADO DEL CLIENTE
	=============================================*/	

	public function ctrRuta(){

		return "https://e-commerce.tonohost.com/";
	
	}

	/*=============================================
	RUTA LADO DEL SERVIDOR
	=============================================*/	

	public function ctrRutaServidor(){

		return "https://adminecommerce.tonohost.com/";
	
	}

}